<?php namespace Drivers\Awia\Factory;
use Drivers\Awia\Entities\ItemEntity;
use Drivers\Awia\Entities\ItemResult;
use Drivers\Awia\Entities\WarehouseItemEntity;
use Drivers\Awia\Errors\DriverException;
use Drivers\Awia\Interfaces\DriverResult;

/**
 * Created by Anton Repin.
 * Date: 7/26/16
 * Time: 5:25 PM
 */

class AwiaItemFactory
{

    public function createSingleItem(DriverResult $result) {

        if($result->getCode() == 200) {

            $item = new ItemEntity();
            $item->fromArray($result->getResult());
            return $item;

        }

        throw new DriverException("Unable to create item from result", $result->getCode());

    }

    public function createItemSet(DriverResult $result) {

        $r = new ItemResult();

        $r->setCode($result->getCode());

        if($result->isNextAvailable())
            $r->__setHasNext();
        
        $a = [];

        if($r->getCode() == 200) {

            if(is_array($result->getResult())) {

                foreach ($result->getResult() as $record) {

                    $i = new ItemEntity();
                    $i->fromArray($record);

                    if (isset($record['warehouse'])){

                        foreach ($record['warehouse'] as $wrecord) {

                            $wi = new WarehouseItemEntity();
                            $wi->fromArray($wrecord);
                            $i->addWarehouseItemEntity($wi);

                        }

                    }

                    $a[] = $i;

                }

            }

        }

        $r->setResult($a);
        return $r;

    }

}