<?php
/**
 * Created by Anton Repin.
 * Date: 8/1/16
 * Time: 6:03 PM
 */

namespace Drivers\Awia\Factory;

use Drivers\Awia\Entities\WarehouseEntity;
use Drivers\Awia\Interfaces\DriverResult;

class AwiaWarehouseFactory
{

    /**
     * @param DriverResult $result
     * @return WarehouseEntity[]
     */
    public function createWarehouseSet(DriverResult $result) {

        $wlist = [];

        if($result->getCode() == 200){

            foreach ($result->getResult() as $w) {

                $whs = new WarehouseEntity();
                $whs->fromArray($w);
                $wlist[] = $whs;

            }

        }
        
        return $wlist;

    }

}