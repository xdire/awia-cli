<?php namespace Drivers\Awia\Errors;

/**
 * Created by Anton Repin.
 * Date: 7/26/16
 * Time: 4:35 PM
 */
class DriverException extends \Exception
{

    /**
     * DriverException constructor.
     * @param string $message
     * @param int    $code
     */
    public function __construct($message, $code)
    {
        parent::__construct($message, $code);
    }

}