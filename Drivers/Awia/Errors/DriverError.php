<?php
/**
 * Created by Anton Repin.
 * Date: 7/26/16
 * Time: 5:03 PM
 */

namespace Drivers\Awia\Errors;

use Drivers\Awia\Interfaces\DriverResultError;

class DriverError implements DriverResultError
{
    
    private $code = 0;
    
    private $message = "";
    
    public function getErrorMessage() : string
    {
        return $this->message;
    }

    public function getErrorCode() : int
    {
        return $this->code;
    }

    public function setErrorMessage(string $message)
    {
        $this->message = $message;
    }

    public function setErrorCode(int $code)
    {
        $this->code = $code;
    }

}