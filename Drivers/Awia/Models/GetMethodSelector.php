<?php namespace Drivers\Awia\Models;
/**
 * Created by Anton Repin.
 * Date: 7/26/16
 * Time: 2:10 PM
 */

class GetMethodSelector {

    private $conn;

    function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    public function itemsWithQuantities(array $items) {

        $this->conn->setMethodUrl("/api/get/items/list");
        $this->conn->setPostData(json_encode($items));
        $this->conn->setRequestType(Connection::REQUEST_POST);
        return new GetMethodParams($this->conn);

    }

    public function itemsWithQuantitiesChangedSince(string $time) {

        $this->conn->setMethodUrl("/api/get/items/changed");
        $this->conn->setQueryStringElement("since",$time);
        $this->conn->setRequestType(Connection::REQUEST_GET);
        return new GetMethodParams($this->conn);

    }

    public function getAllWarehouseData() {

        $this->conn->setMethodUrl("/api/get/warehouse/all");
        return new GetMethodWarehouseDataParams($this->conn);

    }

}