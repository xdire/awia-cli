<?php
/**
 * Created by Anton Repin.
 * Date: 7/26/16
 * Time: 3:57 PM
 */

namespace Drivers\Awia\Models;

use Drivers\Awia\Factory\AwiaItemFactory;

class GetMethodParams
{
    
    private $conn;

    function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function withPage(int $page) {
        $this->conn->setPage($page);
        return $this;
    }

    /**
     * @return \Drivers\Awia\Entities\ItemResult
     * @throws \Drivers\Awia\Errors\DriverException
     */
    public function execute() {
        $f = new AwiaItemFactory();
        return $f->createItemSet($this->conn->exec());
    }

}