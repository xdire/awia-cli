<?php
/**
 * Created by Anton Repin.
 * Date: 7/27/16
 * Time: 12:52 PM
 */

namespace Drivers\Awia\Models;

use Drivers\Awia\Entities\ItemEntity;
use Drivers\Awia\Entities\ItemQtyEntity;

class SetMethodSelector
{

    private $conn;

    function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    public function setNewItem(ItemEntity $e) {

        $this->conn->setMethodUrl("/api/set/item");
        $this->conn->setPostData(json_encode($e->__toAPIArray()));
        $this->conn->setRequestType(Connection::REQUEST_POST);
        return new SetMethodParams($this->conn);

    }

    public function setNewWarehouseForItem(ItemQtyEntity $e) {

        $this->conn->setMethodUrl("/api/set/item/warehouse");
        $this->conn->setPostData(json_encode($e->toArray()));
        $this->conn->setRequestType(Connection::REQUEST_POST);
        return new SetMethodParams($this->conn);

    }

    public function setWarehouseParametersForItem(ItemQtyEntity $e) {

        $this->conn->setMethodUrl("/api/set/item/quantity");
        $this->conn->setPostData(json_encode($e->toArray()));
        $this->conn->setRequestType(Connection::REQUEST_PUT);
        return new SetMethodParams($this->conn);

    }


}