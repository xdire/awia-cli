<?php
/**
 * Created by Anton Repin.
 * Date: 7/26/16
 * Time: 4:38 PM
 */

namespace Drivers\Awia\Models;

use Drivers\Awia\Errors\DriverError;
use Drivers\Awia\Interfaces\DriverResult;
use Drivers\Awia\Interfaces\DriverResultError;

class AwiaResult implements DriverResult
{
    private $code = 0;
    
    private $result = null;

    private $error = null;

    private $next = false;

    public function setCode(int $code)
    {
        $this->code = $code;
    }

    public function getCode() : int
    {
        return $this->code;
    }

    public function setResult(string $result)
    {

        if($data = json_decode($result, true)) {
            $this->result = $data;
        } else {
            if($this->error === null) {
                $e = new DriverError();
                $e->setErrorCode(450);
                $e->setErrorMessage("Result JSON cannot be parsed");
            }
            return;
        }

    }

    public function setParsedResult(array $result)
    {
        $this->result = $result;
    }

    public function getResult() : array
    {
        if($this->result !== null)
            return $this->result;
        else return [];
    }

    public function getError()
    {
        return $this->error;
    }

    public function setError(DriverResultError $error)
    {
        $this->error = $error;
    }

    /**
     * @return boolean
     */
    public function isNextAvailable()
    {
        return $this->next;
    }

    /**
     * 
     */
    public function setNextAvailable()
    {
        $this->next = true;
    }
    
    

}