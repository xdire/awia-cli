<?php
/**
 * Created by Anton Repin.
 * Date: 8/1/16
 * Time: 6:02 PM
 */

namespace Drivers\Awia\Models;

use Drivers\Awia\Factory\AwiaWarehouseFactory;

class GetMethodWarehouseDataParams
{

    private $conn;

    function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    public function execute() {
        $f = new AwiaWarehouseFactory();
        return $f->createWarehouseSet($this->conn->exec());
    }
    
}