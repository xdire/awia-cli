<?php
/**
 * Created by Anton Repin.
 * Date: 7/27/16
 * Time: 2:38 PM
 */

namespace Drivers\Awia\Models;

use Drivers\Awia\Factory\AwiaItemFactory;

class SetMethodParams
{

    private $conn;

    function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @return AwiaResult
     * @throws \Drivers\Awia\Errors\DriverException
     */
    public function execute() {
        return $this->conn->exec();
    }

    /**
     * @return \Drivers\Awia\Entities\ItemEntity
     * @throws \Drivers\Awia\Errors\DriverException
     */
    public function executeWithItemResult() {
        $f = new AwiaItemFactory();
        return $f->createSingleItem($this->conn->exec());
    }

}