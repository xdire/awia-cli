<?php namespace Drivers\Awia\Models;
use Drivers\Awia\Errors\DriverException;

/**
 * Created by Anton Repin.
 * Date: 7/26/16
 * Time: 2:06 PM
 */

class Connection
{

    const REQUEST_GET = 0;
    const REQUEST_POST = 1;
    const REQUEST_PUT = 2;
    const REQUEST_DELETE = 3;

    private $requestType = 0;

    private $requestDefs = ["GET","POST","PUT","DELETE"];

    /** @var string */
    private $baseUrl;
    /** @var string */
    private $methodUrl;
    /** @var string */
    private $parameterUrl;
    /** @var string */
    private $user;
    /** @var string */
    private $key;
    /** @var string */
    private $secret;

    private $queryString = [];

    private $postData = null;

    private $headers = [];

    /** @var int|null */
    private $page = null;

    function __construct(string $url, string $user, string $key, string $secret) {
        $this->baseUrl = $url;
        $this->user = $user;
        $this->key = $key;
        $this->secret = $secret;
    }
    
    public function exec() {

        $result = new AwiaResult();
        $parser = new ResultParser();
        
        $query = [];

        $c = curl_init();

        $url = $this->baseUrl.$this->methodUrl;

        if($this->page !== null) {
            $query["page"] = $this->page;
        }

        if(count($this->queryString) > 0){
            $query = array_merge($query,$this->queryString);
        }

        /* ------------------------------------------------
         *              Create query parameters
         * -----------------------------------------------*/
        $qn = 0;
        $qp = "";
        foreach ($query as $p => $v) {
            if($qn > 0)
                $qp .= "&";
            $qp .= $p."=".urlencode($v);
            $qn++;
        }
        if($qn > 0)
            $url .= "?".$qp;

        /* ------------------------------------------------
         *                      Setup CURL
         * -----------------------------------------------*/
        curl_setopt($c, CURLOPT_URL, $url);

        if($this->requestType > 0) {

            curl_setopt($c, CURLOPT_CUSTOMREQUEST, $this->requestDefs[$this->requestType]);

            if($this->postData !== null) {
                curl_setopt($c, CURLOPT_POSTFIELDS, $this->postData);
            }

        }

        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_HEADER,true);
        //curl_setopt($c, CURLINFO_HEADER_OUT, true);

        if(count($this->headers) > 0)
            curl_setopt($c, CURLOPT_HTTPHEADER, $this->headers);
        /* ------------------------------------------------
         *                      Execute
         * -----------------------------------------------*/
        $response = curl_exec($c);

        if($response === false){
            throw new DriverException("Connection failed", 500);
        }

        /* --------------------- DUMP --------------------*/
        //var_dump($this);

        /* ------------------------------------------------
         *                  Setup Response
         * -----------------------------------------------*/
        //$status = (int) curl_getinfo($c, CURLINFO_HTTP_CODE);
        //$next = curl_getinfo($c, CURLINFO_HEADER_OUT);

        /*var_dump($next);
        var_dump(curl_getinfo($c));
        var_dump($response);*/
        $parser->parseResponse($response);

        $result->setCode($parser->getStatusCode());

        if($parser->getResponseType() == "array")
            $result->setParsedResult($parser->getResponse());
        elseif ($parser->getResponseType() == "string")
            $result->setResult($parser->getResponse());
        
        if($parser->hasNext()) {
            $result->setNextAvailable();
        }

        $this->postData = null;

        curl_close($c);
        return $result;
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param string $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return string
     */
    public function getMethodUrl()
    {
        return $this->methodUrl;
    }

    /**
     * @param string $methodUrl
     */
    public function setMethodUrl($methodUrl)
    {
        $this->methodUrl = $methodUrl;
    }

    /**
     * @return string
     */
    public function getParameterUrl()
    {
        return $this->parameterUrl;
    }

    /**
     * @param $parameter
     */
    public function setParameterUrl($parameter)
    {
        $this->parameterUrl = $parameter;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param string $secret
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }

    /**
     * @return int|null
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int|null $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getRequestType()
    {
        return $this->requestType;
    }

    /**
     * @param int $requestType
     */
    public function setRequestType(int $requestType)
    {
        $this->requestType = $requestType;
    }

    /**
     * @return null
     */
    public function getPostData()
    {
        return $this->postData;
    }

    /**
     * @param null $postData
     */
    public function setPostData($postData)
    {
        $this->postData = $postData;
    }

    /**
     * @return array
     */
    public function getQueryString()
    {
        return $this->queryString;
    }

    /**
     * @param $parameter
     * @param $value
     */
    public function setQueryStringElement($parameter, $value)
    {
        $this->queryString[$parameter] = $value;
    }



}