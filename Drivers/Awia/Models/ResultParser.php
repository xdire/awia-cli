<?php
/**
 * Created by Anton Repin.
 * Date: 8/1/16
 * Time: 12:04 PM
 */

namespace Drivers\Awia\Models;

class ResultParser
{
    /** @var string */
    private $protocol;
    /** @var string */
    private $protocolRaw;
    /** @var string */
    private $protocolVer;
    /** @var bool */
    private $status = false;
    /** @var int */
    private $statusCode = 0;
    /** @var string */
    private $statusExtra = "";
    /** @var int */
    private $contentLength;
    /** @var bool */
    private $hasNext = false;
    /** @var * */
    private $response = null;
    /** @var string */
    private $responseType = null;
    /** @var bool */
    private $responseExist = false;

    /**
     * @return string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * @param string $protocol
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }

    /**
     * @return string
     */
    public function getProtocolRaw()
    {
        return $this->protocolRaw;
    }

    /**
     * @param string $protocolRaw
     */
    public function setProtocolRaw($protocolRaw)
    {
        $this->protocolRaw = $protocolRaw;
    }

    /**
     * @return string
     */
    public function getProtocolVer()
    {
        return $this->protocolVer;
    }

    /**
     * @param string $protocolVer
     */
    public function setProtocolVer($protocolVer)
    {
        $this->protocolVer = $protocolVer;
    }

    /**
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param boolean $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return string
     */
    public function getStatusExtra()
    {
        return $this->statusExtra;
    }

    /**
     * @param string $statusExtra
     */
    public function setStatusExtra($statusExtra)
    {
        $this->statusExtra = $statusExtra;
    }

    /**
     * @return int
     */
    public function getContentLength()
    {
        return $this->contentLength;
    }

    /**
     * @param int $contentLength
     */
    public function setContentLength($contentLength)
    {
        $this->contentLength = $contentLength;
    }

    /**
     * @return boolean
     */
    public function hasNext()
    {
        return $this->hasNext;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @return string
     */
    public function getResponseType()
    {
        return $this->responseType;
    }

    /**
     * @param string $responseType
     */
    public function setResponseType($responseType)
    {
        $this->responseType = $responseType;
    }

    /**
     * @return boolean
     */
    public function isResponseExist()
    {
        return $this->responseExist;
    }

    /**
     * @param boolean $responseExist
     */
    public function setResponseExist($responseExist)
    {
        $this->responseExist = $responseExist;
    }


    /**
     * @param string $response
     * @return bool
     */
    public function parseResponse($response){

        $return = false;

        $r = explode("\n",$response);
        $startContent = false;
        $i = 0;

        foreach($r as $p){

            if($startContent){
                $this->parseContent($p);
            }
            else
            {

                if($i == 0) {

                    $this->parseStatus($p);
                    if($this->statusCode==100) continue;

                } else {
                    $this->parseHeader($p);
                }

            }

            if(strlen($p) < 2 && $i>1) {
                $startContent = true;
            }

            $i++;

        }

        if($startContent && $this->response != null){
            $this->responseExist = true;
        }

        if($i>0){
            $return = true;
        }

        return $return;

    }

    /**
     * @param string $s
     */
    private function parseStatus($s){

        $sh = explode(" ",$s);
        $i=0;

        foreach($sh as $st){

            if($i==0){

                $http = explode("/",$st);
                if(strpos($http[0],'HTTP') !== false){
                    $this->protocol = 'HTTP';
                    $this->protocolRaw = $http[0];
                } else {
                    $this->protocol = 'undefined';
                    $this->protocolRaw = $http[0];
                }

                if(isset($http[1])){
                    $this->protocolVer = $http[1];
                }

            } elseif($i==1) {

                $stat = (int)$st;
                if($stat > 0) {
                    $this->status = true;
                    $this->statusCode = $stat;
                }

            } else {
                $this->statusExtra .= " ".$st;
            }

            $i++;

        }

        $this->statusExtra = trim($this->statusExtra);

    }
    /**
     * @param string $strHeader
     */
    private function parseHeader($strHeader){

        $header = explode(":", $strHeader);
        $name = null;
        $toInt = false;

        if(isset($header[0])) {

            switch(strtolower($header[0])){

                case "contentnext":

                    $this->hasNext = (int)$header[1] == 1 ? true : false;
                    break;

                case "content-length":
                    $name = 'contentLength';
                    $toInt = true;
                    break;

                default:
                    break;

            }

        }

        if(isset($header[1]) && isset($name)){

            if($toInt){
                $this->{$name} = (int)$header[1];
            } else {
                $this->{$name} = $header[1];
            }

        }

    }

    /**
     * @param string $c
     */
    private function parseContent($c){

        if($data = json_decode($c,true)){

            $this->response = $data;
            $this->responseType = 'array';
            $this->responseExist = true;

        } else {

            $this->response = $c;
            $this->responseType = 'string';
            $this->responseExist = true;

        }

    }

}