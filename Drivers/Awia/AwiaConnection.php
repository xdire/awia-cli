<?php namespace Drivers\Awia;
use Drivers\Awia\Models\Connection;
use Drivers\Awia\Models\GetMethodSelector;
use Drivers\Awia\Models\SetMethodSelector;

/**
 * Created by Anton Repin.
 * Date: 7/26/16
 * Time: 2:03 PM
 */
class AwiaConnection
{

    private $connection;

    function __construct($url, $user, $key, $secret)
    {
        $this->connection = new Connection($url, $user, $key, $secret);
    }

    public function get() {
        return new GetMethodSelector($this->connection);
    }
    
    public function set() {
        return new SetMethodSelector($this->connection);
    }

}