<?php namespace Drivers\Awia\Entities;
/**
 * Created by Anton Repin.
 * Date: 7/26/16
 * Time: 5:30 PM
 */

class ItemEntity
{

    /** @var int */
    private $id = null;
    /** @var int */
    private $vendor = null;
    /** @var string */
    private $vendorId = null;
    /** @var string */
    private $upc = null;
    /** @var float */
    private $weight = null;
    /** @var string */
    private $attr1 = null;
    /** @var string */
    private $attr2 = null;
    /** @var string */
    private $attr3 = null;
    /** @var float */
    private $price = null;
    /** @var int */
    private $customId = null;
    /** @var string */
    private $updated = null;

    /** @var WarehouseItemEntity[] */
    private $warehouseList = [];

    public function addWarehouseItemEntity(WarehouseItemEntity $ie) {
        $this->warehouseList[] = $ie;
    }

    /**
     * @return WarehouseItemEntity[]
     */
    public function getWarehouseList()
    {
        return $this->warehouseList;
    }

    public function fromArray(array $data) {

        if(isset($data["vendor"]) && isset($data["vendorId"]) && isset($data["price"])){

            if(isset($data["id"])) {
                $this->id = $data["id"];
            }

            $this->vendor = (int)$data["vendor"];
            $this->vendorId = $data["vendorId"];
            $this->price = (float)$data['price'];

            if(isset($data["gtin"])) {
                $this->upc = $data["gtin"];
            }

            if(isset($data["weight"])) {
                $this->weight = (float)$data["weight"];
            }

            if(isset($data["attr1"])) {
                $this->attr1 = $data['attr1'];
            }

            if(isset($data["attr2"])) {
                $this->attr2 = $data['attr2'];
            }

            if(isset($data["attr3"])) {
                $this->attr3 = $data['attr3'];
            }

            if(isset($data["customId"])) {
                $this->customId = (int)$data['customId'];
            }

        }

    }

    public function toArray() {

        return [
            "id"=>$this->id,
            "vendor"=>$this->vendor,
            "vendorId"=>$this->vendorId,
            "gtin"=>$this->upc,
            "weight"=>$this->weight,
            "attr1"=>$this->attr1,
            "attr2"=>$this->attr2,
            "attr3"=>$this->attr3,
            "price"=>$this->price,
            "upd"=>$this->updated,
            "customId"=>$this->customId,
        ];

    }

    public function __toAPIArray(){

        return [
            "vendor"=>$this->vendor,
            "vendorId"=>$this->vendorId,
            "gtin"=>$this->upc,
            "weight"=>$this->weight,
            "attr1"=>$this->attr1,
            "attr2"=>$this->attr2,
            "attr3"=>$this->attr3,
            "price"=>$this->price,
            "customId"=>$this->customId,
        ];

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param int $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @return string
     */
    public function getVendorId()
    {
        return $this->vendorId;
    }

    /**
     * @param string $vendorId
     */
    public function setVendorId($vendorId)
    {
        $this->vendorId = $vendorId;
    }

    /**
     * @return string
     */
    public function getUpc()
    {
        return $this->upc;
    }

    /**
     * @param string $upc
     */
    public function setUpc($upc)
    {
        $this->upc = $upc;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return string
     */
    public function getAttr1()
    {
        return $this->attr1;
    }

    /**
     * @param string $attr1
     */
    public function setAttr1($attr1)
    {
        $this->attr1 = $attr1;
    }

    /**
     * @return string
     */
    public function getAttr2()
    {
        return $this->attr2;
    }

    /**
     * @param string $attr2
     */
    public function setAttr2($attr2)
    {
        $this->attr2 = $attr2;
    }

    /**
     * @return string
     */
    public function getAttr3()
    {
        return $this->attr3;
    }

    /**
     * @param string $attr3
     */
    public function setAttr3($attr3)
    {
        $this->attr3 = $attr3;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getCustomId()
    {
        return $this->customId;
    }

    /**
     * @param int $customId
     */
    public function setCustomId($customId)
    {
        $this->customId = $customId;
    }

    /**
     * @return string
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param string $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

}