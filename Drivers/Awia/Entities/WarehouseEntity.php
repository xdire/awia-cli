<?php
/**
 * Created by Anton Repin.
 * Date: 8/1/16
 * Time: 6:05 PM
 */

namespace Drivers\Awia\Entities;

class WarehouseEntity
{

    /** @var int */
    private $id = null;
    /** @var int */
    private $vendor = null;
    /** @var string */
    private $vendorName = null;
    /** @var string */
    private $name = null;
    /** @var int */
    private $customer = null;
    /** @var int */
    private $location = null;
    /** @var int */
    private $special = null;
    /** @var string */
    private $state = null;
    /** @var int */
    private $lead = null;

    public function fromArray(array $data) {

        if(isset($data['id'])){
            $this->id = $data['id'];
        }

        if(isset($data['vendor'])){
            $this->vendor = $data['vendor'];
        }

        if(isset($data['vendorName'])){
            $this->vendorName = $data['vendorName'];
        }

        if(isset($data['name'])){
            $this->name = $data['name'];
        }

        if(isset($data['location'])){
            $this->location = $data['location'];
        }

        if(isset($data['special'])){
            $this->special = $data['special'];
        }

        if(isset($data['state'])){
            $this->state = $data['state'];
        }

        if(isset($data['lead'])){
            $this->lead = $data['lead'];
        }

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $wid
     */
    public function setId($wid)
    {
        $this->id = $wid;
    }

    /**
     * @return int
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param int $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param int $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return int
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param int $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return int
     */
    public function getSpecial()
    {
        return $this->special;
    }

    /**
     * @param int $special
     */
    public function setSpecial($special)
    {
        $this->special = $special;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return int
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * @param int $lead
     */
    public function setLead($lead)
    {
        $this->lead = $lead;
    }

    /**
     * @return string
     */
    public function getVendorName()
    {
        return $this->vendorName;
    }

    /**
     * @param string $vendorName
     */
    public function setVendorName($vendorName)
    {
        $this->vendorName = $vendorName;
    }

}