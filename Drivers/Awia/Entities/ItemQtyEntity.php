<?php namespace Drivers\Awia\Entities;
/**
 * Created by Anton Repin.
 * Date: 7/27/16
 * Time: 10:54 AM
 */

class ItemQtyEntity
{

    /** @var int */
    private $vendor = null;
    /** @var string */
    private $vendorId = null;
    /** @var WarehouseItemEntity[] */
    private $warehouse = [];

    function __construct(int $vendor, string $vendorId)
    {
        $this->vendor = $vendor;
        $this->vendorId = $vendorId;
    }

    function setQuantityElement($warehouseId, $quantity=null, $cost=null, $cost1=null) {

        $wi = new WarehouseItemEntity($warehouseId);
        $wi->setWid($warehouseId);

        if($quantity)
            $wi->setQty($quantity);
        if($cost)
            $wi->setCost($cost);
        if($cost1)
            $wi->setCost1($cost1);

        $this->warehouse[] = $wi;

    }

    /**
     * @return array
     */
    public function toArray() {

        $w = [];

        foreach ($this->warehouse as $whs) {
            $w[] = $whs->__toShortArray();
        }

        return [
            "vendor" => $this->vendor,
            "vendorId" => $this->vendorId,
            "warehouse" => $w
        ];

    }

}