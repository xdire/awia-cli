<?php
/**
 * Created by Anton Repin.
 * Date: 7/26/16
 * Time: 5:29 PM
 */

namespace Drivers\Awia\Entities;

class ItemResult
{

    /** @var int */
    private $code;
    /** @var ItemEntity[] */
    private $result;
    /** @var bool */
    private $next = false;

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return ItemEntity[]
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param ItemEntity[] $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    public function isNextAvailable(){
        return $this->next;
    }

    public function __setHasNext() {
        $this->next = true;
    }

}