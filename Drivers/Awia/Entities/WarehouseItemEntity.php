<?php
/**
 * Created by Anton Repin.
 * Date: 7/27/16
 * Time: 11:52 AM
 */

namespace Drivers\Awia\Entities;

class WarehouseItemEntity
{

    /** @var int */
    private $iid = null;
    /** @var int */
    private $wid = null;
    /** @var int  */
    private $qty = null;
    /** @var float */
    private $cost = null;
    /** @var float  */
    private $cost1 = null;
    /** @var string */
    private $updated = null;
    
    public function fromArray(array $data) {

        if(isset($data["wId"]) && isset($data["qty"])){

            if(isset($data["id"]))
                $this->iid = $data["id"];

            $this->wid = $data["wId"];
            $this->qty = $data["qty"];

            if(isset($data["cost"])){
                $this->cost = $data["cost"];
            }

            if(isset($data["cost1"])){
                $this->cost1 = $data["cost1"];
            }

            if(isset($data["upd"])){
                $this->updated = $data["upd"];
            }

        }

    }
    
    /**
     * @return array
     */
    public function __toShortArray() {

        $a = [
            "wId"=> $this->wid
        ];

        if($this->qty !== null)
            $a["qty"] = $this->qty;

        if($this->cost !== null)
            $a["cost"] = $this->cost;

        if($this->cost1 !== null)
            $a["cost1"] = $this->cost1;

        return $a;

    }

    /**
     * @return int
     */
    public function getIid()
    {
        return $this->iid;
    }

    /**
     * @param int $iid
     */
    public function setIid($iid)
    {
        $this->iid = (int)$iid;
    }

    /**
     * @return int
     */
    public function getWid()
    {
        return $this->wid;
    }

    /**
     * @param int $wid
     */
    public function setWid($wid)
    {
        $this->wid = (int)$wid;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param int $qty
     */
    public function setQty($qty)
    {
        $this->qty = (int)$qty;
    }

    /**
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param float $cost
     */
    public function setCost($cost)
    {
        $this->cost = (float)$cost;
    }

    /**
     * @return float
     */
    public function getCost1()
    {
        return $this->cost1;
    }

    /**
     * @param float $cost1
     */
    public function setCost1($cost1)
    {
        $this->cost1 = (float)$cost1;
    }

    /**
     * @return string
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param string $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

}