<?php namespace Drivers\Awia\Interfaces;

/**
 * Created by Anton Repin.
 * Date: 7/26/16
 * Time: 4:44 PM
 */
interface DriverResultError
{
    public function getErrorMessage() : string;

    public function getErrorCode() : int;

    public function setErrorMessage(string $message);

    public function setErrorCode(int $code);
}