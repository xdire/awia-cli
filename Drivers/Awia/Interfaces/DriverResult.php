<?php namespace Drivers\Awia\Interfaces;

/**
 * Created by Anton Repin.
 * Date: 7/26/16
 * Time: 4:41 PM
 */
interface DriverResult
{

    public function setCode(int $code);

    public function getCode() : int;

    public function setResult(string $result);

    public function getResult() : array;

    public function getError();

    public function setError(DriverResultError $error);

    public function setNextAvailable();

    public function isNextAvailable();

}