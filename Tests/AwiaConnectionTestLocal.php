<?php

/**
 * Created by Anton Repin.
 * Date: 8/1/16
 * Time: 11:09 AM
 */
class AwiaConnectionTestLocal extends PHPUnit_Framework_TestCase
{

    public $connUrl = "http://wiaapi";
    public $connUser = "";
    public $connKey = "";
    public $connSecret = "";

    public function testGetItems() {

        require_once ("../app.php");
        $conn = new \Drivers\Awia\AwiaConnection($this->connUrl,$this->connUser,$this->connKey,$this->connSecret);
        $result = $conn->get()->itemsWithQuantities(["B798MM620"])->withPage(1)->execute();

        //var_dump($result);
        $this->assertEquals(200,$result->getCode());

    }

    public function testGetItemsSince() {

        require_once ("../app.php");
        $conn = new \Drivers\Awia\AwiaConnection($this->connUrl,$this->connUser,$this->connKey,$this->connSecret);
        $date = date("Y-m-d H:i:s",(time() - 3600*4) - 3600*5);
        $result = $conn->get()->itemsWithQuantitiesChangedSince($date)->withPage(1)->execute();

        //var_dump($result);
        $this->assertEquals(200,$result->getCode());

    }

    public function testGetItemsSincePaginated() {

        require_once ("../app.php");
        $conn = new \Drivers\Awia\AwiaConnection($this->connUrl,$this->connUser,$this->connKey,$this->connSecret);
        $date = date("Y-m-d H:i:s",(time() - 3600*48) - 3600*5);
        $result = $conn->get()->itemsWithQuantitiesChangedSince($date)->withPage(2)->execute();

        $this->assertEquals(200,$result->getCode(),"Returned results for Pagination test: ".count($result->getResult()));

    }

}
