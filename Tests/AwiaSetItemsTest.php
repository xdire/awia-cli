<?php

/**
 * Created by Anton Repin.
 * Date: 7/27/16
 * Time: 3:03 PM
 */
class AwiaSetItemsTest extends PHPUnit_Framework_TestCase
{

    public $connUrl = "http://45.55.226.124";
    public $connUser = "";
    public $connKey = "";
    public $connSecret = "";

    public function testSetNewItem() {

        require_once ("../app.php");

        $conn = new \Drivers\Awia\AwiaConnection($this->connUrl, $this->connUser, $this->connKey, $this->connSecret);
        $newItem = new \Drivers\Awia\Entities\ItemEntity();

        $newItem->setPrice(60);
        $newItem->setAttr1("style");
        $newItem->setAttr2("size");
        $newItem->setAttr3("color");
        $newItem->setWeight(12);
        $newItem->setUpc("432435656456");

        $newItem->setVendor(7);
        $newItem->setVendorId("NONEOFYOURBUSINESS");

        $result = $conn->set()->setNewItem($newItem)->executeWithItemResult();

        $this->assertEquals(\Drivers\Awia\Entities\ItemEntity::class, get_class($result));

    }

    public function testSetNewWarehouseToItem() {

        require_once ("../app.php");

        $conn = new \Drivers\Awia\AwiaConnection($this->connUrl, $this->connUser, $this->connKey, $this->connSecret);

        $newItem = new \Drivers\Awia\Entities\ItemQtyEntity(7,"NONEOFYOURBUSINESS");
        $newItem->setQuantityElement(31,5,132,435);

        $result = $conn->set()->setNewWarehouseForItem($newItem)->execute();

        $this->assertEquals(200,$result->getCode());

    }

    public function testSetQuantitiyToItemWithWarehouse() {

        require_once ("../app.php");

        $conn = new \Drivers\Awia\AwiaConnection($this->connUrl, $this->connUser, $this->connKey, $this->connSecret);

        $newItem = new \Drivers\Awia\Entities\ItemQtyEntity(7,"NONEOFYOURBUSINESS");
        $newItem->setQuantityElement(31,455466,144);

        $result = $conn->set()->setWarehouseParametersForItem($newItem)->execute();

        $this->assertEquals(200,$result->getCode());

    }

}