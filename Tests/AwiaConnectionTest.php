<?php
/**
 * Created by Anton Repin.
 * Date: 7/26/16
 * Time: 5:12 PM
 */
class AwiaConnectionTest extends PHPUnit_Framework_TestCase
{
    
    public $connUrl = "http://45.55.226.124";
    public $connUser = "";
    public $connKey = "";
    public $connSecret = "";

    public function testGetItems() {
        require_once ("../app.php");
        $conn = new \Drivers\Awia\AwiaConnection($this->connUrl,$this->connUser,$this->connKey,$this->connSecret);
        $result = $conn->get()->itemsWithQuantities(["B798MM620"])->withPage(1)->execute();
        //var_dump($result);
    }

    public function testGetItemsSince() {
        require_once ("../app.php");
        $conn = new \Drivers\Awia\AwiaConnection($this->connUrl,$this->connUser,$this->connKey,$this->connSecret);
        $date = date("Y-m-d H:i:s",time() - 3600);
        $result = $conn->get()->itemsWithQuantitiesChangedSince($date)->withPage(1)->execute();
        var_dump($result);
    }

    public function testGetItemsPaginated() {
        require_once ("../app.php");
        $conn = new \Drivers\Awia\AwiaConnection($this->connUrl,$this->connUser,$this->connKey,$this->connSecret);
    }

    public function testGetItemsSincePaginated() {
        require_once ("../app.php");
        $conn = new \Drivers\Awia\AwiaConnection($this->connUrl,$this->connUser,$this->connKey,$this->connSecret);
    }

}